from flask import Flask, request, Response, jsonify, send_from_directory, abort, make_response
import time
import os
import base64
import csv
from absl import app, logging
import cv2
import numpy as np
import tensorflow as tf

# customize your API through the following parameters
classes_path = './classes/class_dict.csv'
weights_path = './weights/deeplabv3_100epochs_bandung_focalloss_iou.h5'
height = 384                      # size images are resized to for model
width = 576                      # size images are resized to for model
output_path = './detections/'   # path to output folder where images with detections are saved
num_classes = 13                # number of classes in model

# load in weights and classes
physical_devices = tf.config.experimental.list_physical_devices('GPU')
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
model = tf.keras.models.load_model(weights_path, compile=False)

labels = dict()

subset_classes = [
                  'Building',
                  'Tree',
                  'Sky',
                  'Car',
                  'SignSymbol',
                  'Road',
                  'Pedestrian',
                  'Sidewalk',
                  'MotorcycleScooter'
]
with open(classes_path, 'r') as csvin:
  reader = csv.DictReader(csvin)
  for row in reader:
    if row['name'] == 'Train':
      continue
    # labels[row['name']] = (int(row['r']), int(row['g']), int(row['b']))
    if row['name'] in subset_classes:
      labels[row['name']] = (int(row['r']), int(row['g']), int(row['b']))


def int_to_rgb_label(input):
  """
  Input: 0-9
  Output: RGB Labels
  """
  label_seg = np.zeros((input.shape[0], input.shape[1], 3), dtype=np.uint8)
  for i, (label, rgb) in enumerate(labels.items()):
    print(rgb)
    label_seg[np.all(input == i, axis=-1)] = rgb
  
  return label_seg

app = Flask(__name__)

# API that returns image with detections on it
@app.route('/image', methods= ['POST'])
def get_image():
    image = request.files["images"]
    image_name = image.filename
    print(image_name)
    image.save(os.path.join(os.getcwd(), image_name))
    img_raw = tf.image.decode_image(
        open(image_name, 'rb').read(), channels=3)
    img = tf.expand_dims(img_raw, 0)
    img = tf.image.resize(img, (384, 576))

    # img = transform_images(img, size)

    t1 = time.time()
    prediction = model.predict(img)
    prediction = np.argmax(prediction, axis=3)
      
    print(prediction)
    t2 = time.time()
    print('time: {}'.format(t2 - t1))

    print('detections:')
    # for i in range(nums[0]):
    #     print('\t{}, {}, {}'.format(class_names[int(classes[0][i])],
    #                                     np.array(scores[0][i]),
    #                                     np.array(boxes[0][i])))
    # img = cv2.cvtColor(img_raw.numpy(), cv2.COLOR_RGB2BGR)
    result = np.expand_dims(prediction[0], axis=-1)
    result = int_to_rgb_label(result)    
    print(result)
    # img = draw_outputs(img, (boxes, scores, classes, nums), class_names)
    cv2.imwrite(output_path + 'detection.jpg', result)
    print('output saved to: {}'.format(output_path + 'detection.jpg'))
    
    # prepare image for response
    _, img_encoded = cv2.imencode('.png', result)
    response = base64.b64encode(img_encoded.tostring())
    
    #remove temporary image
    os.remove(image_name)

    try:
        return Response(response=response, status=200)
        # return Response(response=response, status=200, mimetype='image/png')
    except FileNotFoundError:
        abort(404)
if __name__ == '__main__':
    app.run(debug=True, host = '0.0.0.0', port=5000)